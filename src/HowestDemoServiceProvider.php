<?php

namespace Howest\Demo;

use Illuminate\Support\ServiceProvider;

class HowestDemoServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadFrom();
        $this->publishFiles();
    }

    protected function packageDir($path){
        return base_path('vendor/howest/demo/') . $path;
    }

    protected function publishFiles()
    {
        $this->publishes([
            $this->packageDir('config/howest.php') => config_path('howest.php'),
            $this->packageDir('resources/lang') => resource_path('lang/vendor/howest')
        ], 'howest-demo');
        $this->publishes([
            $this->packageDir('assets') => public_path('vendor/howest')
        ], 'howest-public');
    }

    protected function loadFrom()
    {
        $this->loadRoutesFrom($this->packageDir('routes/api.php'));
        $this->loadMigrationsFrom($this->packageDir('database/migrations'));
        $this->loadTranslationsFrom($this->packageDir('resources/lang'), 'howest');
    }

}
