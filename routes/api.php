<?php


Route::prefix('api')
    ->middleware('api')
    ->group(function(){

        Route::get("/howest/demo", function(\Howest\Demo\Test\HelloWorld $helloWorld){

            return $helloWorld->say();
        });
    });